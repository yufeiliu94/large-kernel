import keras
import keras.layers as L
from common import BuildingBlock, Up, Stream, Add
from res152 import ResNet152


class Classify(BuildingBlock):
    def __init__(self):
        super().__init__()

    def build(self, *args):
        x, = args
        return L.Conv2D(filters=21, kernel_size=1, padding='same', name=self.basename + 'conv', activation='relu')(x)


def build_fcn_8s():
    x = L.Input(shape=(500, 500, 3), name='data')
    res = ResNet152(padding_input=4).build(x)
    r5 = Stream([res[5], Classify(), Up()])
    r4 = Stream([res[4], Classify(), Add(r5), Up()])
    r3 = Stream([res[3], Classify(), Add(r4), Up()])
    r2 = Stream([res[2], Classify(), Add(r3), Up()])
    r1 = Stream([res[1], Classify(), Add(r2), Up(4)]).build()
    fcn = keras.models.Model(inputs=x, outputs=r1)
    return fcn

if __name__ == '__main__':
    from keras.utils import plot_model
    import os
    import voc

    model = build_fcn_8s()
    if not os.path.exists('fcn-8s.png'):
        plot_model(model, to_file='fcn-8s.png')
    model.compile('sgd', 'categorical_crossentropy', ['accuracy'])
    model.load_weights('resnet152_weights_tf.h5', by_name=True)
    model.fit_generator(voc.train(), steps_per_epoch=400, epochs=10)

