from abc import abstractmethod
import tensorflow.contrib.keras as keras
import numpy as np

L = keras.layers


def bilinear_initializer(shape, dtype=None, *args, **kwargs):
    """
    Make a 2D bilinear kernel suitable for upsampling of the given (h, w) size.
    """
    # TODO handle rect kernel
    assert shape[2] == shape[3]
    size = shape[2]
    factor = (size + 1) // 2
    if size % 2 == 1:
        center = factor - 1
    else:
        center = factor - 0.5
    og = np.ogrid[:size, :size]
    k = (1 - abs(og[0] - center) / factor) * (1 - abs(og[1] - center) / factor)
    return np.array([[k] * shape[1]] * shape[0])


class Counter:
    def __init__(self):
        from collections import defaultdict
        self.counter = defaultdict(int)

    def __getitem__(self, item):
        return self.counter[item]

    def next(self, cls):
        self.counter[cls] += 1
        return self.counter[cls]


counter = Counter()


class BuildingBlock:
    def __init__(self):
        self.serial = counter.next(self.__class__)
        self.basename = '{}-{}/'.format(self.__class__.__name__, self.serial)

    @abstractmethod
    def build(self, *args):
        pass


class Up(BuildingBlock):
    def __init__(self, ratio=2, filters=None, padding='same'):
        super().__init__()
        self.ratio = ratio
        self.filters = filters
        self.padding = padding

    def build(self, *args):
        x, = args
        if self.filters is None:
            self.filters = int(x.shape[-1])
        fn = L.Conv2DTranspose(filters=self.filters, kernel_size=self.ratio * 2, strides=self.ratio,
                               kernel_initializer=bilinear_initializer,
                               padding=self.padding,
                               name=self.basename + 'deconv')
        return fn(x)


class Add(BuildingBlock):
    def __init__(self, lhs):
        super().__init__()
        if isinstance(lhs, BuildingBlock):
            lhs = lhs.build()
        self.lhs = lhs

    def build(self, *args):
        rhs, = args
        w = (int(self.lhs.shape[1]) - int(rhs.shape[1]))
        h = (int(self.lhs.shape[2]) - int(rhs.shape[2]))
        if h < 0 or w < 0:
            self.lhs, rhs = rhs, self.lhs
            w = (int(self.lhs.shape[1]) - int(rhs.shape[1]))
            h = (int(self.lhs.shape[2]) - int(rhs.shape[2]))
        crop = (w // 2 + w % 2, w // 2), (h // 2 + h % 2, h // 2)
        if h == 0 and w == 0:
            return L.Add(name=self.basename + 'add')([self.lhs, rhs])
        else:
            return L.Add(name=self.basename + 'add')([L.Cropping2D(name=self.basename + 'crop', cropping=crop)(self.lhs), rhs])


class Stream(BuildingBlock):
    def __init__(self, blocks):
        super().__init__()
        self.blocks = blocks

    def build(self, *args):
        x = self.blocks[0]
        self.blocks.pop(0)
        if isinstance(x, BuildingBlock):
            x = x.build()
        for block in self.blocks:
            x = block.build(x)
        # TODO remove this money patch
        if not hasattr(x, '_keras_shape'):
            setattr(x, '_keras_shape', tuple(dim.value for dim in x._shape.dims))
        return x
