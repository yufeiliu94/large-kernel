import keras
import keras.layers as L
from common import BuildingBlock, Up, Stream, Add
from resnet50 import ResNet50
import tensorflow as tf


class Classify(BuildingBlock):
    def __init__(self):
        super().__init__()

    def build(self, *args):
        x, = args
        return L.Conv2D(filters=21, kernel_size=1, padding='same', name=self.basename + 'conv',
                        kernel_initializer='he_normal',
                        bias_initializer='he_normal')(x)


def build_fcn_8s():
    input_shape = (500, 500, 3)
    res50 = ResNet50(include_top=False, input_shape=input_shape, input_padding=(5, 5))
    # TODO remove this hack
    offset = 0
    for i in range(10000):
        if res50.get_layer('activation_{}'.format(i)) is not None:
            offset = i - 1
            break
    res = [None] + [res50.get_layer('activation_{}'.format(offset + i)).output for i in [1, 10, 22, 40, 49]]
    print(*res[1:], sep='\n')
    r5 = Stream([res[5], Classify(), Up()]).build()
    r4 = Stream([res[4], Classify(), Add(r5), Up()]).build()
    r3 = Stream([res[3], Classify(), Add(r4), Up()]).build()
    r2 = Stream([res[2], Classify(), Add(r3), Up()]).build()
    r1 = Stream([res[1], Classify(), Add(r2), Up()]).build()
    print(r5, r4, r3, r2, r1, sep='\n')
    fcn = keras.models.Model(inputs=res50.inputs, outputs=r1)
    return fcn


def main():
    from keras.utils import plot_model
    import os
    import voc
    model = build_fcn_8s()
    if not os.path.exists('res50-fcn-8s.png'):
        plot_model(model, to_file='res50-fcn-8s.png')
    model.compile('sgd', 'categorical_crossentropy', ['accuracy'])
    model.fit_generator(voc.train(), steps_per_epoch=400, epochs=10)
    model.save('res50-fcn-8s.h5')

if __name__ == '__main__':
    with tf.device('/cpu:0'):
        main()
