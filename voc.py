import numpy as np
from PIL import Image
import keras
import keras.preprocessing.image as KPI
import itertools
import random

mean = np.array((104.00698793, 116.66876762, 122.67891434))

def load_image(path, resize, resize_policy, dtype):
    im = Image.open(path)
    if resize is not None:
        im = im.resize(resize, resize_policy)
    im = np.array(im)
    if dtype is np.float32:
        im = im.astype(dtype)
        if len(im.shape) is 3:
            im = im[:, :, ::-1]
            im -= mean
        im /= 255
    return im


def load_jpeg(path, resize=(500, 500)):
    return load_image(path, resize, Image.ANTIALIAS, np.float32)


def load_png(path, resize=(500, 500)):
    label = load_image(path, resize, Image.NEAREST, np.int32)
    label *= label != 255
    return label


def load_instance(name, resize=(500, 500)):
    return load_jpeg('data/VOC2012/JPEGImages/{}.jpg'.format(name), resize=resize), \
           load_png('data/VOC2012/SegmentationClass/{}.png'.format(name), resize=resize)


def load_list(path):
    with open(path, encoding='UTF-8') as li:
        return [x.strip() for x in li.readlines()]


def load_subset(name, batch_size=4, resize=(500, 500), num_classes=21, shuffle=True):
    ls = load_list('data/VOC2012/ImageSets/Segmentation/{}.txt'.format(name))
    if shuffle:
        random.shuffle(ls)
    ls = itertools.cycle(ls)
    image = np.zeros([batch_size, *resize, 3])
    label = np.zeros([batch_size, *resize, num_classes])
    while True:
        for i in range(batch_size):
            im, cls = load_instance(next(ls))
            image[i] = im
            label[i] = keras.utils.to_categorical(cls, num_classes=num_classes).reshape((*resize, num_classes))
        yield image, label

def train(batch_size=4, resize=(500, 500), shuffle=True):
    return load_subset('train', batch_size=batch_size, resize=resize, shuffle=shuffle)

def val(batch_size=4, resize=(500, 500), shuffle=True):
    return load_subset('val', batch_size=batch_size, resize=resize, shuffle=shuffle)

if __name__ == '__main__':
    for image, label in load_subset('val'):
        print(image.mean(), image.min(), image.max(), label.max(), label.min())

