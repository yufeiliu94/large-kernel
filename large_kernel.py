import keras
import keras.layers as L
from common import BuildingBlock, Up, Stream, Add
from res152 import ResNet152


class GCN(BuildingBlock):
    def __init__(self, k=11):
        super().__init__()
        self.k = k

    def conv(self, kernel, name):
        return L.Conv2D(filters=21, kernel_size=kernel, padding='same', name=self.basename + name)

    def build(self, *args):
        x, = args
        k = self.k
        lhs = self.conv([k, 1], 'lhs/kx1')(self.conv([1, k], 'lhs/1xk')(x))
        rhs = self.conv([1, k], 'rhs/1xk')(self.conv([k, 1], 'rhs/kx1')(x))
        return L.Add(name=self.basename + 'add')([lhs, rhs])


class BR(BuildingBlock):
    def __init__(self):
        super().__init__()

    def build(self, *args):
        x, = args
        h = L.Conv2D(filters=21, kernel_size=3, padding='same', name=self.basename + 'c2')(
            L.Conv2D(filters=21, kernel_size=3, padding='same', activation='relu', name=self.basename + 'c1')(x))
        return L.Add(name=self.basename + 'add')([x, h])


def build_large_kernel():
    x = L.Input(shape=(512, 512, 3), name='data')
    res_model, res = ResNet152().build(x)
    r5 = Stream([res[5], GCN(), BR(), Up()])
    r4 = Stream([res[4], GCN(), BR(), Add(r5), BR(), Up()])
    r3 = Stream([res[3], GCN(), BR(), Add(r4), BR(), Up()])
    r2 = Stream([res[2], GCN(), BR(), Add(r3), BR(), Up()])
    r1 = Stream([r2, BR(), Up(), BR()]).build()
    lk = keras.models.Model(x, r1)
    return lk


if __name__ == '__main__':
    from keras.utils import plot_model
    import os
    import voc

    model = build_large_kernel()
    if not os.path.exists('large-kernel.png'):
        plot_model(model, to_file='large-kernel.png')
    model.compile('sgd', 'categorical_crossentropy', ['accuracy'])
    model.load_weights('resnet152_weights_tf.h5', by_name=True)
    model.fit_generator(voc.train(), steps_per_epoch=400, epochs=10)

